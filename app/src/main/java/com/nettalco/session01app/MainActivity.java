package com.nettalco.session01app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity {

    //1. Declarar variables globales
    EditText edtNombres;
    EditText edtEdad;
    RadioButton rbMasculino, rbFemenino;
    CheckBox chkTerminos;
    Button btnEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //2. Relacionar mis variables con los IDs del Layout (xml)
        edtNombres = findViewById(R.id.edtNombres);
        edtEdad = findViewById(R.id.edtEdad);
        rbMasculino = findViewById(R.id.rbMasculino);
        rbFemenino = findViewById(R.id.rbFemenino);
        chkTerminos = findViewById(R.id.chkterminos);
        btnEnviar = findViewById(R.id.btnEnviar);
    }
}